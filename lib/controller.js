const UserAuth = require('./UserAuth');
const TokenHandler = require('./token-handler');

const tokenHandler = new TokenHandler();

exports.createAuth = async ctx => {
  let values = ctx.request.body;
  let newAuth = await UserAuth.create(values);

  if (!newAuth || !newAuth._id) {
    ctx.throw(500, 'Error creating user authentication');
  }
  ctx.status = 200;
};

exports.login = async ctx => {
  const { username, password } = ctx.request.body;

  if (!username || !password) {
    ctx.throw(400, 'Missing username or password');
  }

  let userAuth = await UserAuth.findOne({ username });
  if (!userAuth || !userAuth.password) {
    console.error(`Login failed for ${username}, record not found`);
    ctx.throw(401, 'Invalid username or password');
  }
  const isValid = await userAuth.validatePassword(password);
  if (!isValid) {
    console.error(`Login failed for ${username}, password mismatch`);
    ctx.throw(401, 'Invalid username or password');
  }
  // todo: make this a special token that isn't mapped to the username
  ctx.body = { username };
};

exports.signToken = async ctx => {
  const { payload } = ctx.request.body;
  const token = await tokenHandler.sign(payload);
  ctx.body = { token };
};

exports.verifyToken = async ctx => {
  const values = ctx.request.body;
  if (values && values.token) {
    try {
      const payload = await tokenHandler.verify(values.token);
      ctx.body = { valid: true, payload };
    } catch (err) {
      ctx.body = { valid: false };
    }
  } else {
    ctx.throw(400, 'Missing token information');
  }
};

exports.logout = async ctx => {
  ctx.status = 200;
};
